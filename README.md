# LibreArp preset repository

This is a community collection of presets for [LibreArp](https://librearp.gitlab.io/), a free-form pattern arpeggiator.

You may download the presets [as a ZIP file](https://gitlab.com/LibreArp/community-presets/-/archive/main/community-presets-main.zip) or using Git.

The presets are provided free-of-charge under the public domain.

If you would like to contribute your own presets, please contact us via any means listed on our website, or, if you are familiar with Git and GitLab, you may [open a merge request for this repository](https://gitlab.com/LibreArp/community-presets/-/merge_requests) now.
